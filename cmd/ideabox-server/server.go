package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/adlio/trello"
)

func main() {

	// Setup Trello client
	client := trello.NewClient(loadApiKeys())
	list := getTrelloList(client)

	// http.HandleFunc("/", submitPageHandler)
	http.Handle("/", http.FileServer(http.Dir("./views"))) // Entry point to the page.
	http.HandleFunc("/submit", createSubmitProjectHandler(list))
	http.ListenAndServe(":8080", nil)
}

func loadApiKeys() (key, token string) {

	vals := []string{}

	f, _ := os.OpenFile("apikeys.txt", os.O_RDONLY, 0600)
	s := bufio.NewScanner(f)
	for s.Scan() {
		val := strings.Split(s.Text(), "=")
		vals = append(vals, val[1])
	}
	if err := s.Err(); err != nil {
		log.Printf("Error: %s\n", err.Error())
	}
	f.Close()

	key, token = vals[0], vals[1]
	fmt.Printf("API key: %s, token: %s\n", key, token)

	return
}

func getTrelloList(c *trello.Client) (retList *trello.List) {
	// Specify the board to Trello client
	b, err := c.GetBoard("g7KLutSw", trello.Defaults())
	handlerError(err)

	l, err := b.GetLists(trello.Defaults())
	handlerError(err)

	for _, list := range l {
		fmt.Printf("Lists: %+v\n", list)
		if list.Name == "Raakileet" {
			retList = list
		}
	}
	return
}
