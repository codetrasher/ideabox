package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strings"

	"github.com/adlio/trello"
)

func createSubmitProjectHandler(l *trello.List) http.HandlerFunc {

	status := struct {
		Status string
	}{
		Status: "",
	}

	return func(w http.ResponseWriter, r *http.Request) {
		name := r.FormValue("project-name")
		description := r.FormValue("project-description")
		platform := r.FormValue("project-platform")
		contact := r.FormValue("project-contact")

		// Compose card description
		b := strings.Builder{}
		b.WriteString(description)
		fmt.Fprintf(&b, "\n\nPlatform: %s\n", platform)
		fmt.Fprintf(&b, "\nContact: %s\n", contact)

		if err := l.AddCard(&trello.Card{
			Name: name,
			Desc: b.String(),
		}, trello.Defaults()); err != nil {
			// TODO: Write error response
			status.Status = err.Error()
		}
		status.Status = "Card successfully sent! Thank you for your submitting your idea."

		// Build status template
		t, _ := template.ParseFiles("views/status.html")
		t.Execute(w, status)
	}
}

func handlerError(e error) {
	if e != nil {
		fmt.Printf("Error: %s\n", e.Error())
	}
}
